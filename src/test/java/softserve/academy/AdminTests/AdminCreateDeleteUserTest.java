package softserve.academy.AdminTests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.models.User;
import softserve.academy.models.UserDataProvider;
@Test(groups = {"admin", "funcCreateDelete", "user"})
public class AdminCreateDeleteUserTest extends AdminBaseTest{

    @Test(dataProvider = "UserDataProvider")
    public void createDeleteUsers(User user){
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .openCreateModal()
                .fillAllUserFields(user)
                .submitCreateEdit()
                .verifySoftRowWithNamePresentInTab(user.getLastName(), "users")
                .verifyCellWithTextPresentInRow(user.getFirstName())
                .verifyCellWithTextPresentInRow(user.getLocation())
                .verifyCellWithTextPresentInRow(user.getPhotopath())
                .verifyCellWithTextPresentInRow(user.getRole())
                .verifyCellWithTextPresentInRow(user.getLogin())
                .verifyCellWithTextPresentInRow(user.getPassword())
                .deleteRowByName(user.getLastName())
                .verifyRowWithNameNotPresentInTab(user.getLastName(), "users")
                .assertAll();

    }


    @DataProvider(name="UserDataProvider")
    public Object[][] userCreateData() {
        return new Object[][]{
                {UserDataProvider.userToCreate1},
                {UserDataProvider.userToCreate2}
        };
    }




}
