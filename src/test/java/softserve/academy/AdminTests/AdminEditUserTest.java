package softserve.academy.AdminTests;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.GroupDataProvider;
import softserve.academy.models.User;
import softserve.academy.models.UserDataProvider;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;


@Test(groups ={"admin", "funcEdit"})
public class AdminEditUserTest {

    @Test(dataProvider = "UserDataProviderForEditTest")
    public void editUsers(User userBeforeEdited, User userAfterEdit){
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .openEditModal(userBeforeEdited.getLastName())
                .editUserField("lastName", userAfterEdit.getLastName())
                .closeCreateEdit()
                .verifyRowWithNameNotPresentInTab(userAfterEdit.getLastName(), "users")
                .openEditModal(userBeforeEdited.getLastName())
                .editUserField("firstName", userAfterEdit.getFirstName())
                .editUserField("lastName", userAfterEdit.getLastName())
                .editUserField("location", userAfterEdit.getLocation())
                .editUserField("role", userAfterEdit.getRole())
                .editUserField("login", userAfterEdit.getLogin())
                .editUserField("password", userAfterEdit.getPassword())
                .submitCreateEdit()
                .verifySoftRowWithNamePresentInTab(userAfterEdit.getLastName(), "users")
                .verifyCellWithTextPresentInRow(userAfterEdit.getFirstName())
                .verifyCellWithTextPresentInRow(userAfterEdit.getLocation())
                .verifyCellWithTextPresentInRow(userAfterEdit.getRole())
                .verifyCellWithTextPresentInRow(userAfterEdit.getLogin())
                .verifyCellWithTextPresentInRow(userAfterEdit.getPassword())
                .verifyAll();

    }

    @DataProvider(name="UserDataProviderForEditTest")
    public Object[][] userEditData() {
        return new Object[][]{
                {UserDataProvider.userBeforeEdited, UserDataProvider.userAfterEdit}
        };
    }


    User userLogIn = new User("alena", "1234");
    @BeforeTest(groups = {"funcEdit"})
    public void startUserToEdit() {
        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User(userLogIn.getLogin(), userLogIn.getPassword()))
                .waitPageLoaded();
    }
    @BeforeMethod(groups = {"funcEdit"})
    public void createUserToEdit(){
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .openCreateModal()
                .fillAllUserFields(UserDataProvider.userBeforeEdited)
                .submitCreateEdit();
    }

    @AfterMethod(alwaysRun = true, groups = {"funcEdit"})
    public void deleteUserAfterEdit() {
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .deleteRowByName(UserDataProvider.userAfterEdit.getFirstName());
    }
    @AfterTest(alwaysRun = true, groups = {"funcEdit"})
    public void quit(){
        getDriver().quit();
    }
}