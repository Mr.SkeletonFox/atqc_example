package softserve.academy.AdminTests;

import org.testng.annotations.Test;
import softserve.academy.actions.AdminPanelActions;

@Test(groups = {"admin", "smoke"})
public class AdminSmokeTest extends AdminBaseTest {
    
   @Test(description = "Verifies each tabButton and AddButton and EditButton opens corresponding window")
    public void allTabAndCreateEditOpened() {
        new AdminPanelActions()
                .openAdminPanel()
                .openUserTab()
                .verifyOpenedTab("users")
                .openCreateModal()
                .verifyOpened()
                .closeCreateEdit()
                .openEditModal(userLogIn.getLogin())
                .verifyOpened()
                .closeCreateEdit()
                .openGroupTab()
                .verifyOpenedTab("groups")
                .openCreateModal()
                .verifyOpened()
                .closeCreateEdit()
                .openStudentTab()
                .verifyOpenedTab("students")
                .openCreateModal()
                .verifyOpened()
                .closeCreateEdit()
                .escapeToGroupPage()
                .verifyURLContains("Groups");

    }




}
