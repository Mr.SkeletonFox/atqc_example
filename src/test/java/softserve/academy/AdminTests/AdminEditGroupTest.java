package softserve.academy.AdminTests;

import org.testng.annotations.*;
import softserve.academy.actions.AdminPanelActions;
import softserve.academy.actions.LoginPageActions;
import softserve.academy.models.Group;
import softserve.academy.models.GroupDataProvider;
import softserve.academy.models.User;

import static softserve.academy.models.Utils.getDriver;
import static softserve.academy.models.Utils.setDriver;

@Test(groups ={"admin", "funcEdit"})
public class AdminEditGroupTest {

       @Test(dataProvider = "GroupDataProviderForEditTest")
        public void editGroup(Group groupBeforeEdited, Group groupAfterEdit){
            new AdminPanelActions()
                    .openAdminPanel()
                    .openGroupTab()
                 .step("1")
                    .verifySoftInRowWithNameCheckBoxIs(groupBeforeEdited.getName(),groupBeforeEdited.isBudgetOwner())
                    .openEditModal(groupBeforeEdited.getName())
                    .editGroupField("name",groupAfterEdit.getName())
                    .closeCreateEdit()
                    .verifySoftRowWithNameNotPresentInTab(groupAfterEdit.getName(), "groups")
                    .openEditModal(groupBeforeEdited.getName())
                    .editGroupField("name",groupAfterEdit.getName())
                    .editGroupField("direction", groupAfterEdit.getDirection())
                    .editGroupField("location", groupAfterEdit.getLocation())
                    .editGroupField("budgetOwner", String.valueOf(groupAfterEdit.isBudgetOwner()))
                    .editGroupField("startDate", groupAfterEdit.getStartDate())
                    .editGroupField("finishDate", groupAfterEdit.getFinishDate())
                    .editGroupField("teachers", groupAfterEdit.getTeacher())
                    .editGroupField("experts", groupAfterEdit.getExpert())
                    .editGroupField("stage", groupAfterEdit.getStage())
                    .submitCreateEdit()
                 .step("2")
                    .verifySoftInRowWithNameCheckBoxIs(groupAfterEdit.getName(),groupAfterEdit.isBudgetOwner())
                    .openEditModal(groupAfterEdit.getName())
                    .editGroupField("budgetOwner", String.valueOf(groupBeforeEdited.isBudgetOwner()))
                    .submitCreateEdit()
                 .step("3")
                    .verifySoftInRowWithNameCheckBoxIs(groupAfterEdit.getName(),groupBeforeEdited.isBudgetOwner())
                    .verifySoftRowWithNamePresentInTab(groupAfterEdit.getName(), "groups")
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getDirection())
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getLocation())
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getStartDate())
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getFinishDate())
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getTeacher())
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getExpert())
                    .verifyCellWithTextPresentInRow(groupAfterEdit.getExpert())
                    .verifyAll();

        }

        @DataProvider(name="GroupDataProviderForEditTest")
        public Object[][] groupEditData() {
            return new Object[][]{
                    {GroupDataProvider.groupBeforeEdit, GroupDataProvider.groupAfterEdit}
            };
        }

    User userLogIn = new User("alena", "1234");
    @BeforeTest(groups = {"funcEdit"})
    public void startGroupToEdit() {
        setDriver()
                .manage()
                .window()
                .maximize();

        new LoginPageActions()
                .openLoginPage()
                .waitLogInPageLoad()
                .login(new User(userLogIn.getLogin(), userLogIn.getPassword()))
                .waitPageLoaded();
    }
        @BeforeMethod(groups = {"funcEdit"})
        public void createGroupToEdit(){
        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .openCreateModal()
                .fillAllGroupFields(GroupDataProvider.groupBeforeEdit)
                .submitCreateEdit();
    }

    @AfterMethod(alwaysRun = true, groups = {"funcEdit"})
    public void deleteGroupAfterEdit() {
        new AdminPanelActions()
                .openAdminPanel()
                .openGroupTab()
                .deleteRowByName(GroupDataProvider.groupAfterEdit.getName());
    }
        @AfterTest(alwaysRun = true, groups = {"funcEdit"})
        public void quit(){
        getDriver().quit();
    }
}
