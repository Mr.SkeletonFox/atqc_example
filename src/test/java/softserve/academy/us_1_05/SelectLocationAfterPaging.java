package softserve.academy.us_1_05;

import org.testng.annotations.Test;
import softserve.academy.BaseTestK;
import softserve.academy.models.Location;
import softserve.academy.pages.TopMenu;

public class SelectLocationAfterPaging extends BaseTestK {

    SelectLocationAfterPaging(String login, String password) {
        super(login, password);
    }

    @Test(description = "verifies that it is possible to display groups of chosen location when not first page of groups' list was opened before")
    void displayGroupsOfChosenLocationWhenNotFirstPageOfGroupsOpened() {
        new TopMenu()
                .openSelectLocationWindow()
                .selectAllLocations()
                .save()
                .verifyAllGroupsOfChosenLocationDisplayedAfterPriorPaging(Location.DNIPRO);
    }

}
