package softserve.academy.us202;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.SchedulePageActions;
import softserve.academy.models.Filter;
import softserve.academy.pages.TopMenu;
import softserve.academy.us101.BaseTest;

public class AllGroupsButtonFunctionality202Test extends BaseTest {

    @DataProvider(name = "filter")
    public static Object[][] authorizationData() {
        return new Object[][]{
                {Filter.FINISHED },
                {Filter.CURRENT },
                {Filter.PLANNED }
        };

    }


    @Test(groups = {"schedulePage2"}, description = "test allGroups button ability to select " +
            "and deselect all groups and state allGroups button after actions on filter pages",
            dataProvider = "filter")
    public void testFunctionalityAndStateButton(Filter filter) {


        new SchedulePageActions()
                .clickFilter(filter)
                .verifyDefaultStateAllGroupsButton()
                .clickAllGroupsButton()
                .verifyStatePressedAllGroupsButton()
                .isSelectedAll()
                .clickAllGroupsButton()
                .verifyDefaultStateAllGroupsButton()
                .isDeselectedAll()
                .assertAll();

    }
}
