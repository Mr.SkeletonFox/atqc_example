package softserve.academy.us202;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.SchedulePageActions;
import softserve.academy.models.Filter;
import softserve.academy.pages.TopMenu;
import softserve.academy.us101.BaseTest;

public class MultiselectAndDeselectTabsOnFilterPages202Test extends BaseTest {

	@DataProvider(name = "filter")
	public static Object[][] authorizationData() {
		return new Object[][]{
				{Filter.FINISHED},
				{Filter.CURRENT},
				{Filter.PLANNED}
		};

	}

	@Test(groups = {"schedulePage2"}, description = "test verify multiselecting groups' tabs and multideselecting, test",
			dataProvider = "filter")
	public void testMultiselectingAndCounter(Filter filter) {

		new SchedulePageActions()
				.clickFilter(filter)
				.selectAllGroups()
				.isSelectedAll()
				.deselectAllGroups()
				.isDeselectedAll()
				.assertAll();

	}


}
