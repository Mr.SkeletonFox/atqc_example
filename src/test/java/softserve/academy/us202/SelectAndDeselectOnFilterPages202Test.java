package softserve.academy.us202;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.SchedulePageActions;
import softserve.academy.models.Filter;
import softserve.academy.pages.TopMenu;
import softserve.academy.us101.BaseTest;

public class SelectAndDeselectOnFilterPages202Test extends BaseTest {

	@DataProvider(name = "filter")
	public static Object[][] authorizationData() {
		return new Object[][]{
				{Filter.FINISHED,"DP-092-NET"},
				{Filter.CURRENT, "DP-094-MQC"},
				{Filter.PLANNED, "DP-095-JS"}
		};

	}

	@Test(groups = {"schedulePage2"}, description = "test verify select and deselect one group and counter on filter pages",
			dataProvider = "filter")
	public void testSelectingAndDeselectingGroups(Filter filter, String selectGroup) {

		new SchedulePageActions()
				.clickFilter(filter)
				.selectGroup(selectGroup)
				.verifySelectedGroup(selectGroup)
				.verifyСounterOfSelectedGroups()
				.deselectGroup(selectGroup)
				.verifyDeselectedGroup(selectGroup)
				.assertAll();

	}


}
