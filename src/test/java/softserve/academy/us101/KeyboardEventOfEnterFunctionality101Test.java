package softserve.academy.us101;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import softserve.academy.actions.LoginPageActions;

public class KeyboardEventOfEnterFunctionality101Test extends BaseTest {


	@DataProvider(name = "data2")
	public static Object[][] loginData2() {
		return new Object[][]{
				{"1234", "1234"}};

	}


	//button enter functionality
	@Test(groups = {"loginPage"}, description = "test for enter key functionality, ability to save login value" +
			"and clear password field after incorrect data", dataProvider = "data2")
	void testEnterKeyEvent(String login, String password) {


		new LoginPageActions()
				.openLoginPage()
				.waitLogInPageLoad()
				.fillLoginField(login)
				.fillPasswordField(password)
				.pressEnterKey()
				.verifyErrorMessage()
				.assertAll();

	}


}
