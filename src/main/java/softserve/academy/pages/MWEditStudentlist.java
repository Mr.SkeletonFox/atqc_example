package softserve.academy.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

public class MWEditStudentlist extends BasePage {

    @FindBy(xpath = "//*[@class='fa fa-plus-square-o fa-4x createStudent']")
    public WebElement createStudent;

    @FindBy(xpath = "//*[@class='fa fa-times-circle-o fa-3x btn-icon exit']")
    public WebElement iconExit;

    @FindBy(xpath = "//*[@class='fa fa-chevron-right right']")
    public WebElement chevronRight;

    @FindBy(xpath = "//*[@class='fa fa-chevron-left']")
    public WebElement chevronLeft;

    @FindBy(xpath = "//table/tbody/tr")
    public List<WebElement> studentList;

    @FindBy(xpath = "//th[contains(@class,'name')]")
    public WebElement tHadName;

    @FindBy(xpath = "//div[contains(@id,'modal-window')]//th[contains(.,'Photo')]")
    public WebElement tHadPhoto;

    @FindBy(xpath = "//th[contains(@class,'engLevel')]")
    public WebElement tHadEnglishLevel;

    @FindBy(xpath = "//section[contains(@class,'modal-window modal_scoreStudentlist')]//th[contains(@class,'incoming')]")
    public WebElement tHadIncomingTest;

    @FindBy(xpath = "//section[contains(@class,'modal-window modal_scoreStudentlist')]//th[contains(@class,'entry')]")
    public WebElement tHadEntryScore;

    @FindBy(xpath = "//section[contains(@class,'modal-window modal_scoreStudentlist')]//th[contains(.,'Approved by')]")
    public WebElement tHadApprovedBy;


    public MWEditStudentlist() {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
}
