package softserve.academy.models;

public class UserDataProvider {

    // COMMON USE (Currently present in AdminPanel)************************************************************************
    public static final User admin = new User ("Oleg", "Tabakov", Role.ADMIN.getValue(),
            Location.SOFIA.getValue(), "", "Shelenberg", "1234");

    public static final User teacher = new User("Vladislav", "Tikhonov", Role.TEACHER.getValue(),
                            Location.RIVNE.getValue(), "", "Shtirlits", "1234");

    public static final User coordinator = new User("Leonid", "Bronevoy", Role.COORDINATOR.getValue(),
                            Location.KYIV.getValue(), "", "Muler", "1234");

    public static final User admin1 = new User ("Ekaterina", "Gradova", Role.ADMIN.getValue(),
            Location.IVANO_FRANKIVSK.getValue(), "", "radistka", "ket");

    public static final User teacher1 = new User("Evgeniy", "Evstigneev", Role.TEACHER.getValue(),
            Location.LVIV.getValue(), "", "professor", "1234");

    public static final User coordinator1 = new User("Rostislav", "Plyatt", Role.COORDINATOR.getValue(),
            Location.CHERNIVTSY.getValue(), "", "pastor", "1234");
    //Admin TESTS ONLY *******************************************************************************************************************
    public static final User userBeforeEdited = new User("Oleg", "Yankovskiy", Role.ADMIN.getValue(),
            Location.IVANO_FRANKIVSK.getValue(), "/img/batman_icon.png", "Baron", "1234");
    public static final User userAfterEdit = new User("James", "Bond", Role.COORDINATOR.getValue(),
            Location.KYIV.getValue(), "/img/andriy-pereymybida.png", "agent", "007");

    public static final User userToCreate1 = new User("Mikhail", "Kononov", Role.TEACHER.getValue(),
            Location.SOFIA.getValue(), "/img/olexander-reuta.png","nestor", "1234");
    public static final User userToCreate2 = new User("Evgeniy", "Leonov", Role.COORDINATOR.getValue(),
            Location.RIVNE.getValue(), "/img/batman_icon.png", "dotcent", "1234");
    public static final User userToCreate1LoginFromCapital = new User("Mikhail", "Kononov", Role.TEACHER.getValue(),
            Location.SOFIA.getValue(), "/img/olexander-reuta.png","Nestor", "1234");
    //TEST 107 ONLY********************************************************************************************************
    public static User teacher107 = new User("Alex", "Teacher", Role.TEACHER.getValue(),
            Location.RIVNE.getValue(), "", "teach", "1234");
    public static User coordinator107 = new User ("Denys", "Coordinator", Role.COORDINATOR.getValue(),
            Location.KYIV.getValue(), "", "coord", "1234");
    public static final User admin107 = new User ("Borys", "Admin", Role.ADMIN.getValue(),
            Location.LVIV.getValue(), "", "admin", "1234");
    // ******************************************************************************************************************
    //TEST 302 ONLY*******************************************************************************************************
    public static final User teacher106 = new User("Taras","Nikolaiev",Role.TEACHER.getValue(),
            Location.DNIPRO.getValue(),"","taras","1234" );
    // ******************************************************************************************************************
}
